# Written for python 3+

#                  USAGE
#
# python3 sample.py feed -u <login> -p <password>
#                 Requests token and loads feed
#
# python3 sample.py feed -upd true
#                 Updates token and loads feed
#
# python3 sample.py check -lp CA518941
#                 Checks if plate in feed
#

import argparse
import os
import requests
import json
from datetime import datetime, timezone
import iso8601
import hashlib

HOST = "https://api.visecintl.com"

parser = argparse.ArgumentParser()
parser.add_argument('action', type=str, help='Action type')
parser.add_argument('-lp', dest="plate", help='Plate')
parser.add_argument('-u', dest="username", help='Username')
parser.add_argument('-p', dest="password", help='Password')
parser.add_argument('-upd', dest="update", help='Update token', type=bool, default=False)
parser.add_argument('-r', dest="reconnect", help='Get nee token', type=bool, default=False)
args = parser.parse_args()


def _save_token(response_data):
    print("Save token %s" % (response_data["token"]))

    if response_data["error"]["code"] != "NO_ERRORS":
        raise Exception(response_data["error"]["reason"])
    else:
        del response_data["error"]
        with open("token.json", "w") as f:
            f.write(json.dumps(response_data))


def get_token(username, password):
    print("Get token")

    if None in [username, password]:
        raise Exception("Need to get authorization data")

    if all([len(i) > 4 for i in [username, password]]):
        response = requests.post(HOST, data={'query': """
            mutation getToken{
                getAuthToken(input:{login:"%s", password:"%s"}){
                    token,
                    updateCode,
                    expireDate,
                    error{
                        code,
                        reason
                    }
                }
            }
        """ % (username, password)})
        response_data = response.json()["data"]["getAuthToken"]

        _save_token(response_data)
    else:
        raise Exception("Wrong format for username and password")


def update_token(token):
    print("Call token update")

    response = requests.post(HOST, data={'query': """
                mutation updateToken {
                    updateAuthToken(input:{token:"%s",updateCode:"%s"}){
                        token,
                        updateCode,
                        expireDate,
                        error{
                            code,
                            reason
                        }
                    }
                }
            """ % (token["token"], token["updateCode"])})
    response_data = response.json()["data"]["updateAuthToken"]
    _save_token(response_data)
    return load_token()


def load_token():
    with open("token.json", "r") as f:
        token_data = json.loads(f.read())

    expire_date = iso8601.parse_date(token_data["expireDate"])
    now_date = datetime.now(timezone.utc)

    if now_date > expire_date:
        get_token(args.username, args.password)
        return load_token()
    else:
        return token_data


def load_feed(token):
    print("Load feed")

    response = requests.post(HOST, data={'query': """
        query{
            environment(token:"%s"){
                feed{
                    salt,
                    updated,
                    items{
                        edges{
                            node{
                                lipid
                            }
                        }
                    }
                },
                error{
                    code,
                    reason
                }
            }
        }
    """ % (token["token"])})

    response_data = response.json()["data"]["environment"]
    if response_data["error"]["code"] != "NO_ERRORS":
        raise Exception(response_data["error"]["reason"])
    else:
        if response_data["feed"]["updated"]:
            with open("plates.json", "w") as f:
                f.write(json.dumps(response_data["feed"]))

            print("Plates loaded to plates.json")
        else:
            print("No updates")

if args.reconnect:
    get_token(args.username, args.password)

if os.path.exists("token.json"):
    token = load_token()

    if args.update:
        token = update_token(token)

elif None not in [args.username, args.password]:
    get_token(args.username, args.password)
    token = load_token()
else:
    print("Need credentials for sign in")

if args.action == "feed" and "token" in globals().keys():
    load_feed(token)
elif args.action == "check" and args.plate is not None:
    if os.path.exists("plates.json"):
        with open("plates.json", "r") as f:
            plates_data = json.loads(f.read())
            salt = plates_data["salt"]
            plates_data = list([p["node"]["lipid"] for p in plates_data["items"]["edges"]])

            plate_hash = hashlib.sha256(("%s%s" % (args.plate, salt)).encode()).hexdigest()
            if plate_hash in plates_data:
                print("Plate %s is in feed" % args.plate)
            else:
                print("There's no %s in feed" % args.plate)
    else:
        raise Exception("You must load feed data")
